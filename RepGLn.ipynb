{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "be385b2b",
   "metadata": {},
   "source": [
    "# Representations of $GL(n,\\mathbb C)$ at generic n, and $U(n)$ model spectrum"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "06c38cec",
   "metadata": {},
   "source": [
    "We compute tensor products and dimensions of representations of U(n) at generic n. Relevant material can be found in the wikipedia article [Representations of classical Lie groups](https://en.wikipedia.org/wiki/Representations_of_classical_Lie_groups), and in the paper by Koike \"On the decomposition of tensor products of the representations of the classical groups: by means of the universal characters\" referenced therein.\n",
    "\n",
    "In short, irreducible representations of $GL(n,\\mathbb C)$ are labeled by pairs of Young Tableaux $\\lambda\\overline\\mu$, and their tensor product is given by\n",
    "\n",
    "$$\n",
    "V_{\\lambda_1\\overline\\mu_1} \\otimes V_{\\lambda_2\\overline\\mu_2} = \\bigoplus_{\\nu,\\rho} V_{\\nu\\overline\\rho}^{\\oplus \\Gamma^{\\nu\\overline\\rho}_{\\lambda_1\\overline\\mu_1,\\lambda_2\\overline\\mu_2}},\n",
    "$$\n",
    "where $ \\Gamma^{\\nu\\overline\\rho}_{\\lambda_1\\overline\\mu_1,\\lambda_2\\overline\\mu_2} = 0 $ unless $ |\\nu| \\leq |\\lambda_1| + |\\lambda_2|$and $ |\\rho| \\leq |\\mu_1| + |\\mu_2|$. Calling $ l(\\lambda)$ the number of lines in a tableau, if $l(\\lambda_1) + l(\\lambda_2) + l(\\mu_1) + l(\\mu_2) \\leq n $, then \n",
    "$$\n",
    " \\Gamma^{\\nu\\overline\\rho}_{\\lambda_1\\overline\\mu_1,\\lambda_2\\overline\\mu_2} = \\sum_{\\alpha,\\beta,\\eta,\\theta} (\\sum_\\kappa c^{\\lambda_1}_{\\kappa,\\alpha} c^{\\mu_2}_{\\kappa,\\beta})(\\sum_\\gamma c^{\\lambda_2}_{\\gamma,\\eta}c^{\\mu_1}_{\\gamma,\\theta})c^{\\nu}_{\\alpha,\\theta}c^{\\rho}_{\\beta,\\eta},\n",
    "$$\n",
    "where the natural integers $c_{\\lambda,\\mu}^\\nu$ are \n",
    "[Littlewood-Richardson coefficients](https://en.wikipedia.org/wiki/Littlewood%E2%80%93Richardson_rule)."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "878ee6a2",
   "metadata": {},
   "source": [
    "## The ring ${\\rm Sym} \\otimes {\\rm Sym}$ "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "ea124c45",
   "metadata": {},
   "source": [
    "Tensor product decompositions at generic $n$ are most easily performed using the ring of characters ${\\rm Sym} \\otimes {\\rm Sym}$ of GL (which is the projective limit of the sequence of rings of characters of $GL(n)$. A basis of this ring is the set $\\{[\\lambda]\\overline{[\\mu]}\\}$ where $\\lambda,\\mu$ run in the set of integer partitions, and $[\\lambda]\\overline{[\\mu]}$ is the character of $V_{\\lambda\\overline\\mu}$ (this is what I call the mixed tensor basis in the following). A natural basis of ${\\rm Sym}$ is the basis of Schur polynomials which I denote $\\lambda_{GL}$. The two bases described above are related through the following relations :\n",
    "\n",
    "$$\n",
    "[\\lambda,\\mu]_{GL} = \\sum_{\\tau,\\eta,\\nu} (-1)^{|\\tau|} c^{\\lambda}_{\\tau\\eta} c^{\\mu}_{\\tau^t\\nu} \\eta_{GL} \\otimes \\nu_{GL}\n",
    "$$\n",
    "where $\\tau^t$ is the transpose of $\\tau$, and conversely\n",
    "$$\n",
    "\\lambda_{GL} \\otimes \\mu_{GL} = \\sum_{\\tau,\\eta,\\nu} c^{\\lambda}_{\\tau,\\eta} c^{\\mu}_{\\tau,\\nu} [\\eta,\\nu]_{GL}\n",
    "$$\n",
    "\n",
    "Because only the tensor product basis is implemented in Sage, we do all the computations using this basis, then use symbolic calculus to put the result in the mixed tensor basis."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "268aaa94",
   "metadata": {},
   "source": [
    "We introduce two classes, IrrepGLn and GLnChar. Objects of IrrepGLn are irreps of $GL(n)$ i.e. pairs of tableaux. Objects of GLnChar are formal expression of the form $\\sum_{\\lambda,\\mu} c(\\lambda,\\mu)m_{\\lambda\\mu}$, representing GLn characters as sums of irreducible characters.\n",
    "At the time of writing, the notebook doesn't handle Young tableaux whose first line has more than 9 boxes. This could easily be improved if needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "8a9e848e",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Some methods to work with partitions\n",
    "\"\"\"\n",
    "\n",
    "def compact(tup,more_compact=False):\n",
    "    # string of a partition in compact notation\n",
    "    if len(tup)==0:\n",
    "        return ''\n",
    "    if more_compact:\n",
    "        return ''.join(str(v)+':' for v in tup if v>=10)+\\\n",
    "            Partition([v for v in tup if v<10])._repr_compact_high().replace(',','')\n",
    "    else:\n",
    "        return ''.join(str(v)+':' for v in tup if v>=10)+''.join(str(v) for v in tup if v<10)\n",
    "\n",
    "def test_inside(la,mu):\n",
    "    #determines if la is inside mu\n",
    "    for i in range(len(la)):\n",
    "        if(Partition(la).get_part(i)>Partition(mu).get_part(i)):\n",
    "            return False\n",
    "    return True\n",
    "\n",
    "def contained_in(la):\n",
    "    # iterator for the partitions contained in la\n",
    "    for n in range(sum(la)+1):\n",
    "        for eta in Partitions(n,outer = la):\n",
    "            yield eta\n",
    "\n",
    "def contained_in_pair(la,mu):\n",
    "    #iterator for the partitions contained in both la and mu\n",
    "    for n in range(sum(la)+1):\n",
    "        for eta in Partitions(n,outer = la):\n",
    "            if(test_inside(eta,mu)):\n",
    "                yield eta\n",
    "\n",
    "def transpose_contained_in_pair(la,mu):\n",
    "    #iterator for the pairs of partitions (eta,nu) such that (eta,nu.conjugate()) contained in (la,mu)\n",
    "    for n in range(sum(la)+1):\n",
    "        for eta in Partitions(n,outer = la):\n",
    "            if(test_inside(eta.conjugate(),mu)):\n",
    "                yield eta"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "d8cfb87f",
   "metadata": {},
   "outputs": [],
   "source": [
    "s = SymmetricFunctions(QQ).s() # define the Schur basis\n",
    "from sage.libs.lrcalc.lrcalc import lrcoef #Littlewood-Richardson coefficients\n",
    "\n",
    "\n",
    "class IrrepGLn:\n",
    "    \"\"\"Irreducible representations of GLn, and their tensor products (using decomposition in the tensor basis)\"\"\"\n",
    "    \n",
    "    def __init__(self,la,mu):\n",
    "        self.la=Partition(la)\n",
    "        self.mu=Partition(mu)\n",
    "        \n",
    "    def __str__(self,latex=False,more_compact=False):\n",
    "        #print an irrep\n",
    "        if(latex):\n",
    "            return ('['+compact(self.la,more_compact)+']'+'\\\\overline{['+compact(self.mu,more_compact)+']}')\n",
    "        else:\n",
    "            return ('['+compact(self.la,more_compact)+']['+compact(self.mu,more_compact)+']')\n",
    "        \n",
    "    def __eq__(self,other):\n",
    "        if(Partition(self.la)==Partition(other.la) and Partition(self.mu)==Partition(other.mu)):\n",
    "            return True\n",
    "        else:\n",
    "            return False\n",
    "        \n",
    "    def mixed(self):\n",
    "        \"\"\"\n",
    "        Defining the mixed tensor basis i.e. the irreps of GLn from the tensor basis\n",
    "        \"\"\"\n",
    "        return sum((-1)^(tau.size())*lrcoef(self.la,tau,nu)*lrcoef(self.mu,tau.conjugate(),eta)\\\n",
    "                   *tensor([s(nu),s(eta)]) for tau in transpose_contained_in_pair(self.la,self.mu)\\\n",
    "                   for nu in contained_in(self.la) for eta in contained_in(self.mu))\n",
    "    \n",
    "    @staticmethod\n",
    "    def pair_to_var(la,mu):\n",
    "        # change a pair of partitions into a variable of the form 'm_la_mu'\n",
    "        return var('m_'+''.join(str(b) for b in la)+'_'+''.join(str(b) for b in mu))\n",
    "    \n",
    "    def pair_to_mixed(self,la,mu):\n",
    "        # change single term in the tensor space into the mixed basis as a symbolic expression\n",
    "        return sum(lrcoef(la,nu,tau)*lrcoef(mu,eta,tau)*self.pair_to_var(nu,eta)\\\n",
    "                   for tau in contained_in_pair(Partition(la),Partition(mu))\\\n",
    "                   for nu in contained_in(la)\\\n",
    "                   for eta in contained_in(mu))\n",
    "    \n",
    "    def tensor_to_mixed(self,s_tensor_expr):\n",
    "        # change a tensor expression into a symbolic expression in the mixed basis\n",
    "        return sum(c*self.pair_to_mixed(la,mu) for ((la,mu),c) in s_tensor_expr)\n",
    "    \n",
    "    def irrep_to_char(self):\n",
    "        return GLnChar(self.pair_to_var(self.la,self.mu))\n",
    "    \n",
    "    \"\"\"\n",
    "    This finally allows us to compute tensor products in the mixed tensor basis :\n",
    "    \"\"\"\n",
    "    def __mul__(self,other):\n",
    "        #input : \n",
    "        #output : the irrep decomposition of [la1,mu1] x [la2,mu2]\n",
    "        return GLnChar(self.tensor_to_mixed(self.mixed()*other.mixed()))\n",
    "    \n",
    "    #Compute the dimension\n",
    "    @staticmethod\n",
    "    def dla(la):\n",
    "        result=1\n",
    "        for j in range(len(la)):\n",
    "            for i in range(j):\n",
    "                result *= (la[i] - la[j] + j - i)/(j-i)\n",
    "        return result\n",
    "    \n",
    "    @staticmethod\n",
    "    def hookLength(la,i,j):\n",
    "        res = la[i]-j\n",
    "        for k in range(i+1,len(la)):\n",
    "            if(la[k]>j):\n",
    "                res+=1\n",
    "        return res\n",
    "            \n",
    "    def dimension(self):\n",
    "        \"Computed using the formula from El-Samra and King, cf wikipedia for the ref.\"\n",
    "        var('n')\n",
    "        right = prod((n+i+j+1-self.la.conjugate().get_part(i)-self.mu.conjugate().get_part(j))/IrrepGLn.hookLength(self.la,i,j)\\\n",
    "                     for i in range(len(self.la)) for j in range(self.la.get_part(i)))\n",
    "        left = prod((n-i-j-1+self.la.get_part(i)+self.mu.get_part(j))/IrrepGLn.hookLength(self.mu,i,j)\\\n",
    "                     for i in range(len(self.mu)) for j in range(self.mu.get_part(i)))\n",
    "        return left*right\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "47ca37f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "class GLnChar:\n",
    "    \"\"\" Characters of GLn. Characters are written as \\sum coeff * m_la_mu where m_la_mu are vars\"\"\" \n",
    "    \"\"\" representations dare characters with nonnegative integer coefficients\"\"\"\n",
    "\n",
    "    def __init__(self,char):\n",
    "        self.char=char\n",
    "        \n",
    "    @staticmethod\n",
    "    def term(R,c,more_compact=False,latex=False):\n",
    "        # string representing a single term in compact notation\n",
    "        if c==-1:\n",
    "            termstr = '-'+R.__str__(latex,more_compact)\n",
    "        elif c!=1:\n",
    "            termstr = '%s'%(c) + R.__str__(latex,more_compact)\n",
    "        else:\n",
    "            termstr = R.__str__(latex,more_compact)\n",
    "\n",
    "        return termstr\n",
    "    \n",
    "    def coefficient(self,irrep):\n",
    "        return self.char.coefficient(irrep)\n",
    "    \n",
    "    @staticmethod\n",
    "    def var_to_rep(v):\n",
    "        # change a variable of the form 'm_la_mu' into a rep (la,mu)\n",
    "        sv = str(v)\n",
    "        la = [int(sv[i]) for i in range(2,sv.find('_',2))]\n",
    "        mu = [int(sv[i]) for i in range(sv.find('_',2)+1,len(sv))]\n",
    "        return IrrepGLn(la,mu)\n",
    "    \n",
    "    def char_to_list(self):\n",
    "        #split an expression like (\\sum coeff * m_la_mu) into a list of [coeff,irrep]\n",
    "        res1=[]\n",
    "        res2=[]\n",
    "        temp = str(self.char).replace('-','+ -1*').split('+')\n",
    "        for m in temp:\n",
    "            res1.append(m.split('*'))\n",
    "            if(len(m.split('*'))==1):\n",
    "                res1[-1].insert(0,'1')\n",
    "        for m in res1:\n",
    "            res2.append([int(m[0].strip()),self.var_to_rep(var(m[1].strip()))])\n",
    "        return res2\n",
    "    \n",
    "    @staticmethod\n",
    "    def sort_irreps(a):\n",
    "        #sort pairs of tableaux appearing in a character according to 1) their total size\n",
    "        #2) the difference in the size of their first line 3) the size of their first line\n",
    "        la=Partition(a[1].la)\n",
    "        mu=Partition(a[1].mu)\n",
    "        if(la==[]):\n",
    "            la=[0]\n",
    "        if(mu==[]):\n",
    "            mu=[0]\n",
    "        return (max(la[0],mu[0]),-abs(la[0]-mu[0]),sum(x for x in la),la[0])\n",
    "        \n",
    "    def __str__(self,more_compact=False,latex=False):\n",
    "        #the latex argument produces a result that can be copy pasted in latex\n",
    "        l=self.char_to_list()\n",
    "        # string representing a character in compact notation\n",
    "        return '+'.join(self.term(R,c,more_compact,latex) for (c,R) in \\\n",
    "                        sorted(l,key=self.sort_irreps,reverse=True)).replace('+-','-')\n",
    "    \n",
    "    def __add__(self,other):\n",
    "        return GLnChar(self.char+other.char)\n",
    "    \n",
    "    def __mul__(self,other):\n",
    "        res=GLnChar(0)\n",
    "        l1=self.char_to_list()\n",
    "        l2=other.char_to_list()\n",
    "        for term1 in l1:\n",
    "            for term2 in l2:\n",
    "                for i in range(term1[0]):\n",
    "                    for j in range(term2[0]):\n",
    "                        res = res + (term1[1]*term2[1])\n",
    "        return res\n",
    "    def dimension(self):\n",
    "        return factor(sum(term[0]*term[1].dimension() for term in self.char_to_list()))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "d4763bf7",
   "metadata": {},
   "source": [
    "The classes IrrepGLn and GLnChar allow to compute tensor products of representations of $GL(n)$. Below are a few examples showing how to use them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "8ac8175d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[21][31]+[111][31]+[11][3]+[21][211]+[21][22]+[2][21]+[111][211]+[111][22]+3[11][21]+[1][2]+[11][111]+[1][11]\n",
      "[2][1]+2[1][1]+[][1]\n",
      "[2][2]+[2][11]+2[1][2]+[1][1]+2[1][11]+2[][1]\n"
     ]
    }
   ],
   "source": [
    "if __name__ == '__main__' :\n",
    "    R1=IrrepGLn([1],[1])\n",
    "    R2=IrrepGLn([1,1],[2,1])\n",
    "    print(R1*R2)\n",
    "    c1=GLnChar(2*m_1_1+m_2_1)\n",
    "    c2=GLnChar(m__1)\n",
    "    print(c1+c2)\n",
    "    print(c1*c2)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "3bf20fe0",
   "metadata": {},
   "source": [
    "They also compute the dimensions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d99cd7f1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1/640*(n + 6)*(n + 3)^2*(n + 2)*(n + 1)*(n - 1)*(n - 2)*(n - 3)*n^2\n",
      "1/2*(n + 6)*(n + 1)*(n - 1)\n"
     ]
    }
   ],
   "source": [
    "if __name__ == '__main__' :\n",
    "    R=IrrepGLn([4,2],[3,1])\n",
    "    print(R.dimension())\n",
    "    print(c1.dimension())"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "31ad57e9",
   "metadata": {},
   "source": [
    "## Spectrum of the U(n) model "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "d980edc2",
   "metadata": {},
   "source": [
    "We compute the decomposition into irreducibles of the following representations that enter the spectrum of the U(n) model :\n",
    "\n",
    "$$\n",
    "\\Lambda_r = \\sum_{k=0}^{r-1} (-1)^k [r-k,1^k]\n",
    "$$\n",
    "\n",
    "and\n",
    "\n",
    "$$\n",
    "    \\Omega_{(r,s)} = \\delta_{r,1}[] + \\frac{1}{r}\\sum_{r'=0}^{r-1} e^{2i\\pi r's} U_{r \\wedge r'} \\left(\\Lambda_{\\frac{r}{r\\wedge r'}} \\otimes \\overline{\\Lambda_{\\frac{r}{r\\wedge r'}}}-2\\right)  = \\delta_{r,1} [] + \\frac{1}{r} \\sum_{g,g'|r, g g'=r} \\varphi_{rs} (g) U_{g'} (\\Lambda_g \\otimes \\overline{\\Lambda_g} -2)\n",
    "$$\n",
    "\n",
    "where the $U_d$'s are polynomials defined for $d \\in \\mathbb N$ via $U_d(q+q^{-1}) = q^d + q^{-d}$, and $\\varphi_k(r) = \\sum_{l=1}^r \\delta_{r \\wedge l ,1 } e^{2i \\pi \\frac{kl}{r}}$ is a Ramanujan sum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "ebf08953",
   "metadata": {},
   "outputs": [],
   "source": [
    "def phi_k(k,r):\n",
    "    \"\"\"\n",
    "    A Ramanujan sum phi_k(r) = sum_{d|(k^r)} mu(r/d)*d\n",
    "    This is Kluyver's formula from 1906 as stated on the Wikipedia page\n",
    "    \"\"\"\n",
    "    return sum(moebius(r/d)*d for d in divisors(gcd(k,r)))\n",
    "\n",
    "def U_d(d, z):\n",
    "    \"\"\"\n",
    "    The Chebshev U_d(z) polynomial.\n",
    "    Remark: 2*chebyshev_T(d, x/2) == U_d(x) from the paper\n",
    "    \"\"\"\n",
    "    return 2*chebyshev_T(d,z/2)\n",
    "\n",
    "def hook(r,k):\n",
    "    return [r-k]+[1]*k\n",
    "\n",
    "def Lambda_x_barLambda(r):\n",
    "    return sum(sum((-1)^(k+kp) * tensor([s(hook(r,k)),s(hook(r,kp))]) for k in range(r)) for kp in range(r))\n",
    "\n",
    "def Omega(r,ss):\n",
    "    R=IrrepGLn([],[])\n",
    "    res_tensor = 1/r * (sum(phi_k(r*ss,g)*U_d(r/g,Lambda_x_barLambda(g)\\\n",
    "                                        -2*tensor([s([]),s([])])) for g in divisors(r)))\n",
    "    if r==1:\n",
    "        res_tensor+= tensor([s([]),s([])])\n",
    "    return GLnChar(R.tensor_to_mixed(res_tensor))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "6e32cc93",
   "metadata": {},
   "outputs": [],
   "source": [
    "def display_Omega_rs(r,s,more_compact=False,latex=False):\n",
    "    if latex:\n",
    "        print('\\\\Omega_{(%s,%s)} & ='%(r,s) + Omega(r,s).__str__(more_compact,latex) + '\\\\\\\\')\n",
    "    else:\n",
    "        print('Om_(%s,%s) ='%(r,s) + Omega(r,s).__str__(more_compact,latex))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "fa47a32f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Om_(1,0) =[1][1]\n",
      "Om_(2,0) =[2][2]+[1^2][1^2]\n",
      "Om_(2,1/2) =[2][1^2]+[1^2][2]\n",
      "Om_(3,0) =[3][3]+[3][1^3]+[1^3][3]+2[21][21]+[2][2]+[2][1^2]+[1^2][2]+[1^3][1^3]+[1^2][1^2]+[1][1]+[][]\n",
      "Om_(3,1/3) =[3][21]+[21][3]+[21][21]+[2][2]+[21][1^3]+[1^3][21]+[2][1^2]+[1^2][2]+[1^2][1^2]+[1][1]\n",
      "Om_(3,2/3) =[3][21]+[21][3]+[21][21]+[2][2]+[21][1^3]+[1^3][21]+[2][1^2]+[1^2][2]+[1^2][1^2]+[1][1]\n"
     ]
    }
   ],
   "source": [
    "if __name__ == '__main__' :\n",
    "    for r in [1,2,3]:\n",
    "        for q in list(range(r)):\n",
    "            display_Omega_rs(r,q/r,more_compact=True,latex=False)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "a8ab78f8",
   "metadata": {},
   "source": [
    "# Four-point invariants\n",
    "\n",
    "For four given representations $\\lambda_1,\\lambda_2,\\lambda_3,\\lambda_4$, we determine three representations $\\Lambda_s,\\Lambda_t,\\Lambda_u$ associated to the three partitions of $\\{1,2,3,4\\}$ into subsets of two elements. If $\\lambda_1\\otimes \\lambda_2 = \\bigoplus_\\lambda N_{12}^\\lambda \\lambda $, we define \n",
    "$$\n",
    "\\Lambda_s  = \\bigoplus_\\lambda N_{12}^\\lambda N_{34}^\\lambda \\lambda \\quad , \\quad \n",
    "\\Lambda_t = \\bigoplus_\\lambda N_{14}^\\lambda N_{23}^\\lambda \\lambda\n",
    "\\quad , \\quad \n",
    "\\Lambda_u = \\bigoplus_\\lambda N_{13}^\\lambda N_{24}^\\lambda \\lambda\n",
    "$$\n",
    "Defining the size of a representation as $\\|\\bigoplus_\\lambda m^\\lambda \\lambda \\|=\\sum_\\lambda m^\\lambda$, we then have\n",
    "$$\n",
    "\\|\\Lambda_s\\| = \\|\\Lambda_t\\| = \\|\\Lambda_u\\| = \\dim \\text{Hom}\\left(\\otimes_{i=1}^4 \\lambda_i, []\\right)\n",
    "$$\n",
    "i.e. the number of four-point invariants. The representations $\\Lambda_s,\\Lambda_t,\\Lambda_u$ encode three different bases of the space of four-point invariants. In examples we take $\\lambda_i$ to be representations of the type $\\Omega_{(r_i,s_i)}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "34929c33",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Channel s : number of irreps is 9\n",
      "[2][2]+[2][11]+[11][2]+[11][11]+4[1][1]+[][]\n",
      "Channel t : number of irreps is 9\n",
      "[2][2]+[2][11]+[11][2]+[11][11]+4[1][1]+[][]\n",
      "Channel u : number of irreps is 9\n",
      "[2][2]+[2][11]+[11][2]+[11][11]+4[1][1]+[][]\n"
     ]
    }
   ],
   "source": [
    "class FourPoint:\n",
    "    \n",
    "    permutations = {'s': [0, 1, 2, 3], 't': [1, 2, 3, 0], 'u': [1, 3, 2, 0]}\n",
    "    \n",
    "    def __init__(self,chars):\n",
    "        \"\"\" \n",
    "        chars = an array of four characters.\n",
    "        \"\"\"\n",
    "        self.chars = chars\n",
    "        self.channels = {}\n",
    "        for key, val in FourPoint.permutations.items():\n",
    "            char1 = chars[val[0]] * chars[val[1]]\n",
    "            char2 = chars[val[2]] * chars[val[3]]\n",
    "            self.channels[key]=0\n",
    "            for c,R in char1.char_to_list():\n",
    "                m=R.pair_to_var(R.la,R.mu)\n",
    "                self.channels[key] = self.channels[key] + char2.coefficient(m)*c*m\n",
    "            self.channels[key]=GLnChar(self.channels[key])\n",
    "\n",
    "    def display(self):\n",
    "        for key, val in self.channels.items():\n",
    "            print('Channel', key, ': number of irreps is', sum(c for c,R in val.char_to_list()))\n",
    "            print(val)\n",
    "\n",
    "if __name__ == '__main__':\n",
    "\n",
    "    indices01 = [(1, 0)]*4\n",
    "    indices02 = [(2, 0)] + [(1, 0)]*3\n",
    "    indices03 = [(2, 1/2)] + [(1, 0)]*3\n",
    "    indices04 = [(2, 0)]*2 + [(1, 0)]*2\n",
    "    indices05 = [(2, 0),(2,1/2)] + [(1, 0)]*2\n",
    "    indices06 = [(2, 1/2)]*2 + [(1, 0)]*2\n",
    "    indices07 = [(3, 0)] + [(1, 0)]*3\n",
    "    indices08 = [(3, 1/3)] + [(1, 0)]*3\n",
    "    indices09 = [(3, 2/3)] + [(1, 0)]*3\n",
    "    indices10 = [(2,0)]*3 + [(1,0)]\n",
    "    indices11 = [(2,0)]*2 + [(2,1/2)] + [(1,0)]\n",
    "    indices12 = [(2,0)] + [(2,1/2)]*2 + [(1,0)]\n",
    "    indices13 = [(2,1/2)]*3 + [(1,0)]\n",
    "    indices14 = [(3,0)] + [(2,0)] + [(1,0)]*2\n",
    "    indices15 = [(3,0)] + [(2,1/2)] + [(1,0)]*2\n",
    "    indices16 = [(3,1/3)] + [(2,0)] + [(1,0)]*2\n",
    "    indices17 = [(3,1/3)] + [(2,1/2)] + [(1,0)]*2\n",
    "    indices18 = [(3,2/3)] + [(2,0)] + [(1,0)]*2\n",
    "    indices19 = [(3,2/3)] + [(2,1/2)] + [(1,0)]*2\n",
    "    indices20 = [(2,0)]*3 + [(1,0)]\n",
    "    indices21 = [(2,0)]*2 + [(2,1/2)] + [(1,0)]\n",
    "    indices22 = [(2,0)] + [(2,1/2)]*2 + [(1,0)]\n",
    "    indices23 = [(2,1/2)]*3 + [(1,0)]\n",
    "    fp = FourPoint([Omega(*pair) for pair in indices01])\n",
    "    fp.display()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "40664ee1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2][2]+[1][1]+[][]\n"
     ]
    }
   ],
   "source": [
    "print(IrrepGLn([2],[])*IrrepGLn([],[2]))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "9b5f0758",
   "metadata": {},
   "source": [
    "## Relation to the O(n) spectrum"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "e2e5d040",
   "metadata": {},
   "source": [
    "$O(n)$ being a subgroup of $GL(n)$, we can decompose irreps of the latter into irreps of the former. The branching rules are\n",
    "$$\n",
    "\\lambda\\bar\\mu = \\sum m^{\\lambda\\bar\\mu}_{\\nu} \\nu^{O(n)},\\quad m^{\\lambda\\bar\\mu}_{\\nu} = \\sum_{\\alpha,\\beta,\\gamma,\\delta} c^{\\nu}_{\\alpha\\beta} c^{\\lambda}_{\\alpha\\,2\\gamma}c^{\\mu}_{\\beta\\,2\\delta}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "3af37470",
   "metadata": {},
   "outputs": [],
   "source": [
    "SymmetricFunctions(QQ['Q','n'].fraction_field()).inject_shorthands('all',verbose=False)\n",
    "(Q,n) = s.base_ring().gens()\n",
    "from sage.libs.lrcalc.lrcalc import lrcoef\n",
    "\n",
    "def mlamunu(la,mu,nu):\n",
    "    #sum(for alpha contained_in(nu) for beta contained_in(nu)\n",
    "    return sum(sum(sum(sum(lrcoef(nu,al,be)*lrcoef(la,al,[i*2 for i in ga])*lrcoef(mu,be,[i*2 for i in de]) for al in contained_in(nu))\n",
    "               for be in contained_in(nu)) for ga in contained_in(la)) for de in contained_in(mu))\n",
    "\n",
    "def IrrepGLntoOn(R):\n",
    "    p=(Partition(R.la).size() + Partition(R.mu).size())%2\n",
    "    q=(Partition(R.la).size() + Partition(R.mu).size())//2+1\n",
    "    return sum(sum(mlamunu(R.la,R.mu,nu)*o(nu) for nu in Partitions(2*n+p)) for n in range(q))\n",
    "\n",
    "def charGLntoOn(char):\n",
    "    list=char.char_to_list()\n",
    "    res=0\n",
    "    for term in list:\n",
    "        res+=term[0]*IrrepGLntoOn(term[1])\n",
    "    return res"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "e750024e",
   "metadata": {},
   "source": [
    "As an example, we decompose $[3]\\overline{[21]}$ and $[1]\\overline{[1]} + [3]\\overline{[1^2]}$ into representations of $O(n)$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "7b9d76da",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "o[1, 1] + o[2] + o[2, 1, 1] + o[2, 2] + 2*o[3, 1] + o[3, 2, 1] + o[4] + o[4, 1, 1] + o[4, 2] + o[5, 1]\n",
      "o[1, 1] + o[1, 1, 1] + o[2] + o[2, 1] + o[3, 1, 1] + o[4, 1]\n"
     ]
    }
   ],
   "source": [
    "if __name__ == '__main__':\n",
    "    R=IrrepGLn([3],[2,1])\n",
    "    print(IrrepGLntoOn(R))\n",
    "    c=GLnChar(m_1_1+m_3_11)\n",
    "    print(charGLntoOn(c))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 10.0",
   "language": "sage",
   "name": "SageMath-10.0"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
