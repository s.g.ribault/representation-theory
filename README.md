# Representations of $GL(n)$, $O(n)$ and $S_Q$ with generic parameters

We compute the tensor structure of the Deligne category of the general linear group $GL(n)$, of the orthogonal group $O(n)$ and of the symmetric group $S_Q$. We then write the spaces of states of the $U(n)$, $O(n)$ and $Q$-state Potts models in terms of objects of these categories. We also compute the branching rules from the Brauer to the unoriented Jones-Temperley-Lieb algebra, which is an alternative way of deriving the space of states of the $O(n)$ model.

Irreducible representations of $GL(n)$ (or simple objects in the Deligne category) are indexed by bipartitions. Irreducible representations of $O(n)$ and $S_Q$ are indexed by partitions. The parameters $n$, $Q$ are assumed to be generic i.e. not integer. 

The Jupyter notebooks RepGLn.ipynb and RepOnSQ.ipynb run with SageMath 9.2 +. 
